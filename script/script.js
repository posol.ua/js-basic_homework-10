/*Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

document.createElement() - цей метод створює новий елемент DOM з заданим ім'ям тегу. 

document.createTextNode() - цей метод створює новий текстовий вузол з вказаним текстом.

element.innerHTML - встановлює або отримує HTML або XML вміст внутрішнього HTML елемента.

element.insertAdjacentHTML() - цей метод вставляє вказаний текст HTML або вузол під час виконання 
вказаної HTML-розмітки відносно визначеного елемента та вказаної позиції відносно цього елемента.

element.cloneNode() - цей метод створює копію вказаного вузла з включенням або виключенням дочірніх вузлів, але без обробки подій клонованих елементів.

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

Отримання посилання на елементи з класом "navigation"
const nav = document.querySelector('.navigation');

Вибір елемента для видалення
const elNavigation = nav[i];

Видалення елемента зі сторінки
elNavigation.parentNode.removeChild(elNavigation);

3.Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

el.before(node,strings)
el.after(node,strings)
el.append(node,strings)
el.prepend(node,strings)
або
insertAdjacentHTML('beforebegin', html); insertAdjacentHTML('afterend', html)

Практичні завдання
1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.*/

const link = document.createElement('a');
link.textContent = 'Learn More';
link.href = '#';
const footer = document.querySelector('footer');
footer.append(link);

/*2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.*/

const main = document.querySelector('main');
const newSelect = document.createElement('select');
newSelect.id = 'rating';
main.prepend(newSelect);

const options = [
    { value: "4", text: "4 Stars" },
    { value: "3", text: "3 Stars" },
    { value: "2", text: "2 Stars" },
    { value: "1", text: "1 Star" }
  ];
  
  options.forEach( option => {
    const newOption = document.createElement("option");
    newOption.value = option.value;
    newOption.textContent = option.text;
    newSelect.append(newOption);
});

  

